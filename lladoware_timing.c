// lladoware_timing.c
// 
// Timing functions for TI TM4C123GXL using Keil v5
// A simple library that contains timer and delay functions
// 
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-06

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "lladoware_timing.h"

////////////////////////////////////////////////////////////////////////////////
// wait_sec()
// Delays for the specified number of microseconds

void wait_sec(uint32_t num_sec){
    uint32_t count = SYS_FREQ/6*num_sec;
    while(count-- > 0){}
}

////////////////////////////////////////////////////////////////////////////////
// wait_ms()
// Delays for the specified number of milliseconds

void wait_ms(uint32_t num_ms){
    uint32_t count = SYS_FREQ/1000/6*num_ms;
    while(count-- > 0){}
}

////////////////////////////////////////////////////////////////////////////////
// wait_us()
// Delays for the specified number of microseconds

void wait_us(uint32_t num_us){
    uint32_t count = SYS_FREQ/1000000/6*num_us;
    while(count-- > 0){}
}

////////////////////////////////////////////////////////////////////////////////
// End of file
