// commutation.c
// 
// motor control for TI TM4C123GXL using Keil v5
// Program controls power electronics required to provide position, speed, and 
// torque control for a brushless DC motor
// 
// This file contains code that manages motor commutation, sending appropriate 
// signals to each motor phase based on motor position
// 
// This file is part of motor_control v0.3
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-03

#include "lladoware_system_config.h"

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define INDICATOR_LEDS  1

////////////////////////////////////////////////////////////////////////////////
// Hardware configuration parameters

#define WINDING_1P  PB4
#define WINDING_1N  PB5
#define WINDING_2P  PB6
#define WINDING_2N  PB7
#define WINDING_3P  PE4
#define WINDING_3N  PE5

#define MOTOR_PWM_FREQ      25000   // Must be >> 1kHz due to RC filter time constant of 1ms
#define COMMUTATION_RANGE   3600    // Arbitrary

#define MOTOR_PWM_PERIOD    SYS_FREQ/PWM_DIV/MOTOR_PWM_FREQ

////////////////////////////////////////////////////////////////////////////////
// commutation_init()
// Initializes hardware and logic required for motor commutation

void commutation_init(void);

////////////////////////////////////////////////////////////////////////////////
// commutation_update()
// Adjusts commutation for new desired position

void commutation_update(uint32_t position);

////////////////////////////////////////////////////////////////////////////////
// End of file
