// main.c
// 
// Car audio control interface for TI TM4C123GXL using Keil v5
// Program generates analog voltage control signals for Audiotec Fischer DSP 
// and modulates those voltages based on button input from user. Eventually 
// button input from user will be replaced with CANBUS input from car.
// 
// This main file defines the system's high level behavior.
// 
// This file is part of car_audio_interface v0.1
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-03

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h> // must precede other #include's, gives us integers
#include "PLL.h"
#include "tm4c123gh6pm.h"

#include "commutation.h"

// from startup.s
void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Global Constants

#define CTRL_FREQ       25000   // Same as PWM frequency
#define POSITION_STEP   5       // Motor steps per PWM tick

#define CTRL_PERIOD     SYS_FREQ/CTRL_FREQ

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void Timer1A_Init(uint32_t period, uint32_t priority);
void Timer1A_Handler(void);

////////////////////////////////////////////////////////////////////////////////
// main()

int main(void) {
    // Initialize all hardware
    PLL_Init();
    commutation_init();
    Timer1A_Init(CTRL_PERIOD, 1);

    // spin forever
    while(1) {
        // All further actions are performed by interrupt handlers
    }
}

////////////////////////////////////////////////////////////////////////////////
// Timer1A_Init()
// Initializes all hardware needed to use Timer 1A for frame update

void Timer1A_Init(uint32_t period, uint32_t priority){
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R1;                      // activate TIMER1A
        while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R1) == 0) {}           // wait for timer to start
        TIMER1_CTL_R = 0x00;                                            // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00;                                            // set to 32-bit mode
        TIMER1_TAMR_R = 0x02;                                           // set to periodic mode
        TIMER1_TAILR_R = period - 1;                                    // set reset value
        TIMER1_TAPR_R = 0;                                              // set bus clock resolution
        TIMER1_ICR_R = 0x01;                                            // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x01;                                           // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (priority << 29);    // set priority
        NVIC_EN0_R = 1 << 21;                                           // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x01;                                            // enable TIMER1A
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer1A_Handler
// Increments desired position and updates commutation accordingly

void Timer1A_Handler(void) {
    static uint32_t position = 0;

    TIMER1_ICR_R = 0x01;    // acknowledge timer1A timeout

    position = (position + POSITION_STEP) % COMMUTATION_RANGE;
    commutation_update(position);
}

////////////////////////////////////////////////////////////////////////////////
// End of file
