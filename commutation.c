// commutation.c
// 
// Motor control for TI TM4C123GXL using Keil v5
// Program controls power electronics required to provide position, speed, and 
// torque control for a brushless DC motor
// 
// This file contains code that manages motor commutation, sending appropriate 
// signals to each motor phase based on motor position
// 
// This file is part of motor_control v0.3
// Travis Llado, travis@travisllado.com
// Last modified 2020-04-06

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h> // must precede other #include's, gives us integers

#include "commutation.h"
#include "lladoware_output_pins.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define NUM_PHASES  6

const uint32_t phase_size = COMMUTATION_RANGE / NUM_PHASES;
const uint32_t peak = MOTOR_PWM_PERIOD - 1;
uint32_t phase_initialized = NUM_PHASES + 1;

#if INDICATOR_LEDS
    #define RED     0
    #define GREEN   1
    #define BLUE    2
#endif

////////////////////////////////////////////////////////////////////////////////
// commutation_init()
// Initializes hardware and logic required for motor commutation

void commutation_init(void){
    init_pin(WINDING_1P, MOTOR_PWM_PERIOD);
    update_pin(WINDING_1P, HIGH);

    init_pin(WINDING_1N, MOTOR_PWM_PERIOD);
    update_pin(WINDING_1N, LOW);

    init_pin(WINDING_2P, MOTOR_PWM_PERIOD);
    update_pin(WINDING_2P, HIGH);

    init_pin(WINDING_2N, MOTOR_PWM_PERIOD);
    update_pin(WINDING_2N, LOW);

    init_pin(WINDING_3P, MOTOR_PWM_PERIOD);
    update_pin(WINDING_3P, HIGH);

    init_pin(WINDING_3N, MOTOR_PWM_PERIOD);
    update_pin(WINDING_3N, LOW);

    #if INDICATOR_LEDS
        onboard_leds_init();
    #endif
}

////////////////////////////////////////////////////////////////////////////////
// commutation_update()
// Adjusts commutation for new desired position

void commutation_update(uint32_t position){
    uint32_t phase_num = position / phase_size;

    switch(phase_num){
        case 0:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_3P, HIGH);
                update_pin(WINDING_1P, LOW);
                onboard_leds_red_update(HIGH);
            }
            break;
        case 1:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_2N, LOW);
                update_pin(WINDING_3N, HIGH);
                onboard_leds_red_update(LOW);
            }
            break;
        case 2:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_1P, HIGH);
                update_pin(WINDING_2P, LOW);
                onboard_leds_green_update(HIGH);
            }
            break;
        case 3:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_3N, LOW);
                update_pin(WINDING_1N, HIGH);
                onboard_leds_green_update(LOW);
            }
            break;
        case 4:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_2P, HIGH);
                update_pin(WINDING_3P, LOW);
                onboard_leds_blue_update(HIGH);
            }
            break;
        case 5:
            if(phase_initialized != phase_num){
                phase_initialized = phase_num;
                update_pin(WINDING_1N, LOW);
                update_pin(WINDING_2N, HIGH);
                onboard_leds_blue_update(LOW);
            }
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
